import RPi.GPIO as GPIO
import cv2
import time
import numpy as np
import copy
import master
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

### DEFINE PINS/PWM/SENSORS
## ULTRASONIC
TRIG_FRONT = 16 # GPIO 23 CONFIRMED WORKING
ECHO_FRONT = 15 # GPIO 22 CONFIRMED WORKING
TRIG_LEFT = 18  # GPIO 24
#ECHO_LEFT = 22 # GPIO 25
TRIG_RIGHT = 5  # GPIO 3 (SCL) CONFIRMED WORKING
ECHO_RIGHT = 7  # GPIO 4 (GPCLK0) CONFIRMED WORKING
## LIFT
LIFT_WHITE = 22 #3  # GPIO 2 (SDA) CONFIRMED WORKING
LIFT_LOWER_LIMIT = 11 #24 #29 #11   # GPIO 17 CONFIRMED WORKING
LIFT_UPPER_LIMIT = 13   # GPIO 27 CONFIRMED WORKING
GPIO.setup(LIFT_WHITE, GPIO.OUT)
WHITE_PWM = GPIO.PWM(LIFT_WHITE, 62.5)
# LIFT PARAMETERS
LIFT_NEUTRAL = 9.4
LIFT_LOWER_MAX = 6.
LIFT_RAISE_MAX = 12.25
## DRIVE
STEER = 8      # GPIO 14 (TXD) CONFIRMED WORKING
DRIVE = 10      # GPIO 15 (RXD) CONFIRMED WORKING
GPIO.setup(STEER, GPIO.OUT)
STEER_PWM = GPIO.PWM(STEER, 50)
GPIO.setup(DRIVE, GPIO.OUT)
DRIVE_PWM = GPIO.PWM(DRIVE, 62.5)
# DRIVE PARAMETERS
STEER_NEUTRAL = 6.8
STEER_LEFT_MAX = 9
STEER_RIGHT_MAX = 4.6
DRIVE_NEUTRAL = 7.4 #7.5
DRIVE_FORWARD_MAX = 7.7
DRIVE_REVERSE_MAX = 7.0
DRIVE_CONSTANT = 0.75
## CAMERA
FRONT_CAM = cv2.VideoCapture(0) # CONFIRMED WORKING
# CAMERA PARAMETERS
WIDTH = 640
HEIGHT = 480
FRONT_CAM.set(3, WIDTH)
FRONT_CAM.set(4, HEIGHT)
R_LOWER = np.array([0, 100, 100], np.uint8)
R_UPPER = np.array([10, 255, 255], np.uint8)
B_LOWER = np.array([110, 50, 50], np.uint8)
B_UPPER = np.array([130, 255, 255], np.uint8)
G_LOWER = np.array([36, 25, 25], np.uint8)
G_UPPER = np.array([70, 255, 255], np.uint8)


if __name__ == "__main__":
    #lift_demo()
    #init_lift()
    #raise_lift()
    #   lift_demo()
#   init_lift()
#   while True:
#       print("Lower switch: ", GPIO.input(LIFT_LOWER_LIMIT))
#       time.sleep(0.01)
    master.init_drive()
    #print("REVERSE")
    #DRIVE_PWM.ChangeDutyCycle(DRIVE_NEUTRAL + 0.6*(DRIVE_FORWARD_MAX - DRIVE_NEUTRAL))
    #time.sleep(3)
    #print("NEUTRAL")
    #DRIVE_PWM.ChangeDutyCycle(DRIVE_NEUTRAL)
    #time.sleep(3)
    print("FORWARD")
    DRIVE_PWM.ChangeDutyCycle(7.25)
    time.sleep(10)
    DRIVE_PWM.ChangeDutyCycle(DRIVE_NEUTRAL)
    time.sleep(3)
