import RPi.GPIO as GPIO
import time
servo_pin = 11
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(servo_pin, GPIO.OUT)
servo_pwm = GPIO.PWM(servo_pin, 50)
servo_pwm.start(0)

while True:
	dc = input("Enter duty cycle: ")
	#dc += 1
	#if (dc == 8): dc = 5
	servo_pwm.ChangeDutyCycle(float(dc))
	time.sleep(0.1)
