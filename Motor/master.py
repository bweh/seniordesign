import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)

### DEFINE PINS/PWM
## ULTRASONIC
TRIG_FRONT = 11	# GPIO 17
ECHO_FRONT = 13	# GPIO 27
TRIG_BACK = 15	# GPIO 22
ECHO_BACK = 16	# GPIO 23
TRIG_LEFT = 18	# GPIO 24
ECHO_LEFT = 22	# GPIO 25
TRIG_RIGHT = 29	# GPIO 05
ECHO_RIGHT = 31	# GPIO 06
## LIFT
LIFT_WHITE = 36         # GPIO 16
LIFT_LOWER_LIMIT = 37   # GPIO 26
GPIO.setup(LIFT_WHITE, GPIO.OUT)
WHITE_PWM = GPIO.PWM(LIFT_WHITE, 62.5)

def init_us_array():
    print("Initializing ultrasonic array...")
    GPIO.setup(TRIG_FRONT, GPIO.OUT)
    GPIO.setup(ECHO_FRONT, GPIO.IN)
    GPIO.setup(TRIG_BACK, GPIO.OUT)
    GPIO.setup(ECHO_BACK, GPIO.IN)
    GPIO.setup(TRIG_LEFT, GPIO.OUT)
    GPIO.setup(ECHO_LEFT, GPIO.IN)
    GPIO.setup(TRIG_RIGHT, GPIO.OUT)
    GPIO.setup(ECHO_RIGHT, GPIO.IN)
	
def measure(TRIG, ECHO):
    GPIO.output(TRIG, True)
    time.sleep(0.00001)
    GPIO.output(TRIG, False)
    while GPIO.input(ECHO) == 0:
        start_time = time.time()
    while GPIO.input(ECHO) != 0:
        end_time = time.time()
    return (end_time - start_time) * 17150
    
def monitor():
    print("Monitoring ultrasonics...")
    front_dist = measure(TRIG_FRONT, ECHO_FRONT)
    back_dist = measure(TRIG_BACK, ECHO_BACK)
    left_dist = measure(TRIG_LEFT, ECHO_LEFT)
    right_dist = measure(TRIG_RIGHT, ECHO_RIGHT)
    all_dist = {"FRONT": front_dist, "BACK": back_dist, "LEFT": left_dist, "RIGHT": right_dist}
    print("Measurements: ", all_dist)
    
def init_lift():
    print("Initializing lift motor...")
    GPIO.setup(LIFT_LOWER_LIMIT, GPIO.IN)
    WHITE_PWM.start(9.4)
    
def lower_lift():
    print("Lowering...")
    WHITE_PWM.ChangeDutyCycle(6)
    while GPIO.input(LIFT_LOWER_LIMIT) == 0:
        time.sleep(0.01)
    WHITE_PWM.ChangeDutyCycle(9.4)
    
def raise_lift():
    print("Raising...")
    WHITE_PWM.ChangeDutyCycle(12.25)
    time.sleep(3)
    WHITE_PWM.ChangeDutyCycle(9.4)

if __name__ == "__main__":
    init_us_array()
    init_lift()
    time.sleep(5)
    while True:
        lower_lift()
        time.sleep(0.5)
        monitor()
        raise_lift()
        time.sleep(0.5)
    