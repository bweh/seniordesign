import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)

TRIG_PIN = 13
ECHO_PIN = 15

def init_ultrasonic():
	GPIO.setup(TRIG_PIN, GPIO.OUT)
	GPIO.setup(ECHO_PIN, GPIO.IN)
	GPIO.output(TRIG_PIN, False)	

def measure():
	GPIO.output(TRIG_PIN, True)
	time.sleep(0.00001) # 10 us start command
	GPIO.output(TRIG_PIN, False)
	while GPIO.input(ECHO_PIN) == 0:
		start_time = time.time() # Log current time
	while GPIO.input(ECHO_PIN) != 0:
		end_time = time.time() # Log echo time	
	return ((end_time - start_time) * 17150)

init_ultrasonic()
while True:
	time.sleep(0.5)
	print("Measuring distance...")
	msg = "Distance: {dist} cm"
	print(msg.format(dist = measure()))

