import RPi.GPIO as GPIO
from time import sleep

ledpin = 18				# PWM pin connected to LED
GPIO.setwarnings(False)			#disable warnings
GPIO.setmode(GPIO.BOARD)		#set pin numbering system
GPIO.setup(ledpin,GPIO.OUT)
pi_pwm = GPIO.PWM(ledpin,50)		#create PWM instance with frequency
pi_pwm.start(0)				#start PWM of required Duty Cycle 
duty= 2
while True:
	duty= float(input())
	pi_pwm.ChangeDutyCycle(duty) #provide duty cycle in the range 0-100
	sleep(0.01)
    
    
