"""
@file hough_lines.py
@brief This program demonstrates line finding with the Hough transform
"""
import sys
import math
import cv2 as cv
import numpy as np

def main():
    
    cap = cv.VideoCapture(0)
    while(1): 
        ret, src = cap.read()          
        dst = cv.Canny(src, 50, 200, None, 3)        
        # Copy edges to the images that will display the results in BGR
        cdst = cv.cvtColor(dst, cv.COLOR_GRAY2BGR)
        cdstP = np.copy(cdst)     
        linesP = cv.HoughLinesP(dst, 1, np.pi / 180, 50, None, 50, 10)
        
        if linesP is not None:
            for i in range(0, len(linesP)):
                l = linesP[i][0]
                cv.line(cdstP, (l[0], l[1]), (l[2], l[3]), (0,0,255), 3, cv.LINE_AA)
                print("Line",i, "(x1,y1)=(",l[0], "," ,l[1],"),", "(x2,y2)=(",l[2], "," ,l[3],")", "slope=", (l[3]-l[1])/(l[2]-l[0]))
        
        cv.imshow("Source", src)
        cv.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)
        
        k = cv.waitKey(5) & 0xFF
        if k == 27:
            break

    # Destroys all of the HighGUI windows.
    cv2.destroyAllWindows()
 
    # release the captured frame
    cap.release()

    
if __name__ == "__main__":
    main()
