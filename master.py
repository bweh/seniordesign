import RPi.GPIO as GPIO
import cv2
import time
import numpy as np
import copy
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

### DEFINE PINS/PWM/SENSORS
## ULTRASONIC
TRIG_FRONT = 16 # GPIO 23 CONFIRMED WORKING
ECHO_FRONT = 15 # GPIO 22 CONFIRMED WORKING
TRIG_LEFT = 18  # GPIO 24
#ECHO_LEFT = 22 # GPIO 25
TRIG_RIGHT = 5  # GPIO 3 (SCL) CONFIRMED WORKING
ECHO_RIGHT = 7  # GPIO 4 (GPCLK0) CONFIRMED WORKING
## LIFT
LIFT_WHITE = 22 #3  # GPIO 2 (SDA) CONFIRMED WORKING
LIFT_LOWER_LIMIT = 13 #24 #29 #11   # GPIO 17 CONFIRMED WORKING
LIFT_UPPER_LIMIT = 11   # GPIO 27 CONFIRMED WORKING
GPIO.setup(LIFT_WHITE, GPIO.OUT)
WHITE_PWM = GPIO.PWM(LIFT_WHITE, 62.5)
# LIFT PARAMETERS
LIFT_NEUTRAL = 9.4
LIFT_LOWER_MAX = 6.
LIFT_RAISE_MAX = 12.25
## DRIVE
STEER = 8      # GPIO 14 (TXD) CONFIRMED WORKING
DRIVE = 10      # GPIO 15 (RXD) CONFIRMED WORKING
GPIO.setup(STEER, GPIO.OUT)
STEER_PWM = GPIO.PWM(STEER, 50)
GPIO.setup(DRIVE, GPIO.OUT)
DRIVE_PWM = GPIO.PWM(DRIVE, 62.5)
# DRIVE PARAMETERS
STEER_NEUTRAL = 6.8
STEER_LEFT_MAX = 9
STEER_RIGHT_MAX = 4.6
DRIVE_NEUTRAL = 7.8 #7.4 or 7.5 or 7before
DRIVE_FORWARD_MAX = 7.1 
DRIVE_REVERSE_MAX = 6.8 #7 before
DRIVE_CONSTANT = 0.5
## CAMERA
FRONT_CAM = cv2.VideoCapture(0) # CONFIRMED WORKING
# CAMERA PARAMETERS
WIDTH = 640
HEIGHT = 480
FRONT_CAM.set(3, WIDTH)
FRONT_CAM.set(4, HEIGHT)
R_LOWER = np.array([0, 100, 100], np.uint8)
R_UPPER = np.array([10, 255, 255], np.uint8)
B_LOWER = np.array([100, 40, 40], np.uint8)
B_UPPER = np.array([140, 255, 255], np.uint8)

G_LOWER = np.array([36, 25, 25], np.uint8)
G_UPPER = np.array([70, 255, 255], np.uint8)

### ULTRASONIC
def init_us_array():
    print("Initializing ultrasonic array...")
    #GPIO.setup(TRIG_BACK, GPIO.OUT)
    #GPIO.setup(ECHO_BACK, GPIO.IN)
    #GPIO.setup(TRIG_LEFT, GPIO.OUT)
    #GPIO.setup(ECHO_LEFT, GPIO.IN)
    #GPIO.setup(TRIG_RIGHT, GPIO.OUT)
    #GPIO.setup(ECHO_RIGHT, GPIO.IN)
    
def measure(TRIG, ECHO):
    GPIO.output(TRIG, True)
    time.sleep(0.00001)
    GPIO.output(TRIG, False)
    while GPIO.input(ECHO) == 0:
        start_time = time.time()
    core_count = 0
    while GPIO.input(ECHO) != 0:
        #core_count += 1
        #if core_count == 1000:
        #   return 200.
        end_time = time.time()
    return (end_time - start_time) * 17150

def monitor():
    print("Monitoring ultrasonics...")
    front_dist = measure(TRIG_FRONT, ECHO_FRONT)
    time.sleep(0.005)
    left_dist = measure(TRIG_LEFT, ECHO_LEFT)
    time.sleep(0.005)
    right_dist = measure(TRIG_RIGHT, ECHO_RIGHT)
    all_dist = {"FRONT": front_dist, "LEFT": left_dist, "RIGHT": right_dist}
    print("Measurements: ", all_dist)

### LIFT
def init_lift():
    print("Initializing lift motor...")
    GPIO.setup(LIFT_LOWER_LIMIT, GPIO.IN, GPIO.PUD_UP)
    GPIO.setup(LIFT_UPPER_LIMIT, GPIO.IN, GPIO.PUD_UP)
    WHITE_PWM.start(LIFT_NEUTRAL)
    time.sleep(0.1)
    WHITE_PWM.ChangeDutyCycle(LIFT_NEUTRAL)
    time.sleep(3)

def lower_lift():
    print("Lowering...")
    WHITE_PWM.ChangeDutyCycle(LIFT_LOWER_MAX)
    while GPIO.input(LIFT_LOWER_LIMIT) == 1:
        time.sleep(0.05)
    print("Lower limit detected...")
    WHITE_PWM.ChangeDutyCycle(LIFT_NEUTRAL)
    time.sleep(0.5)

def raise_lift():
    print("Raising...")
    WHITE_PWM.ChangeDutyCycle(LIFT_RAISE_MAX)
    while GPIO.input(LIFT_UPPER_LIMIT) == 1:
        time.sleep(0.05)
    print("Upper limit detected...")
    WHITE_PWM.ChangeDutyCycle(LIFT_NEUTRAL)
    time.sleep(0.5)

def lift_demo():
    print("Lifting demo...\nLIFT_NEUTRAL: %3.2f LIFT_LOWER_MAX: %3.2f LIFT_RAISE_MAX: %3.2f" % (LIFT_NEUTRAL, LIFT_LOWER_MAX, LIFT_RAISE_MAX))
    init_lift()
    for i in range(3):
        raise_lift()
        lower_lift()

    print("Demo concluded...")

### DRIVE
def init_drive():
    print("Initializing drive motor...")
    DRIVE_PWM.start(DRIVE_NEUTRAL) 

def init_steer():
    print("Initializing steering servo...")
    STEER_PWM.start(STEER_NEUTRAL) 
    
def forward():
    print("Forward...")
    DRIVE_PWM.ChangeDutyCycle(DRIVE_FORWARD_MAX * DRIVE_CONSTANT) # 2 ms / 20 ms = 10%
    time.sleep(2.)
    DRIVE_PWM.ChangeDutyCycle(DRIVE_NEUTRAL) 
    
def backward():
    print("Backward...")
    DRIVE_PWM.ChangeDutyCycle(DRIVE_REVERSE_MAX*DRIVE_CONSTANT) 
    time.sleep(2.)
    DRIVE_PWM.ChangeDutyCycle(DRIVE_NEUTRAL) 

def left():
    print("Left...")
    #DRIVE_PWM.ChangeDutyCycle(7.5)
    STEER_PWM.ChangeDutyCycle(STEER_LEFT_MAX)
    time.sleep(3.)
    STEER_PWM.ChangeDutyCycle(STEER_NEUTRAL) 
    
def right():
    print("Right...")
    #DRIVE_PWM.ChangeDutyCycle(7.5)
    STEER_PWM.ChangeDutyCycle(STEER_RIGHT_MAX)
    time.sleep(3.)
    STEER_PWM.ChangeDutyCycle(STEER_NEUTRAL) 

def steer_spectrum():
    print("Steer spectrum...")
    for i in np.arange(STEER_RIGHT_MAX, STEER_LEFT_MAX, 0.1):
        STEER_PWM.ChangeDutyCycle(i)
        time.sleep(0.1)
    time.sleep(1)
    for i in np.arange(STEER_LEFT_MAX, STEER_RIGHT_MAX, -0.1):
        STEER_PWM.ChangeDutyCycle(i)
        time.sleep(0.1)
    time.sleep(1)
    STEER_PWM.ChangeDutyCycle(STEER_NEUTRAL)

def steer_demo():
    print("Steering demo...\nSTEER_NEUTRAL: %3.2f STEER_LEFT_MAX: %3.2f STEER_RIGHT_MAX: %3.2f" % (STEER_NEUTRAL, STEER_LEFT_MAX, STEER_RIGHT_MAX))
    init_steer()
    time.sleep(1)
    for i in range(3):
        left()
        time.sleep(1)
        right()
        time.sleep(1)
    steer_spectrum()
    print("Demo concluded...")
    
def drive_demo():
    print("Driving demo...\nDRIVE_NEUTRAL: %3.2f DRIVE_FORWARD_MAX: %3.2f DRIVE_REVERSE_MAX: %3.2f" % (DRIVE_NEUTRAL, DRIVE_FORWARD_MAX, DRIVE_REVERSE_MAX))
    init_drive()
    print("Neutral...")
    time.sleep(5)
    forward()
    print("Neutral...")
    time.sleep(2)
    backward()
    print("Neutral...")
    time.sleep(2)   
    print("Demo concluded...")
    
def drive_steer_demo():
    print("Driving-Steering demo...\nSTEER_NEUTRAL: %3.2f STEER_LEFT_MAX: %3.2f STEER_RIGHT_MAX: %3.2f" % (STEER_NEUTRAL, STEER_LEFT_MAX, STEER_RIGHT_MAX))
    print("Driving-Steering demo...\nDRIVE_NEUTRAL: %3.2f DRIVE_FORWARD_MAX: %3.2f DRIVE_REVERSE_MAX: %3.2f" % (DRIVE_NEUTRAL, DRIVE_FORWARD_MAX, DRIVE_REVERSE_MAX))
    init_drive()
    init_steer()
    time.sleep(1)
    DRIVE_PWM.ChangeDutyCycle(DRIVE_FORWARD_MAX)
    steer_spectrum()
    DRIVE_PWM.ChangeDutyCycle(DRIVE_NEUTRAL)
    time.sleep(2)
    print("Demo concluded...")

### CAMERA
def live_feed_raw_demo():
    print("Front camera demo...\nPress (q) to quit...\nTermination without (q) results in floating device (requires reboot)")
    #init_us_array()
    frame_num = 0
    while True:
        frame_num += 1
        __, frame = FRONT_CAM.read()
        cv2.imshow('FRONT_CAM_VIEW',frame)
        #if frame_num % 10 == 0:
        #   measurement = measure(TRIG_FRONT, ECHO_FRONT)
        #   print("FRONT (cm): %3.2f" % measurement)
        time.sleep(0.02)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    FRONT_CAM.release()
    cv2.destroyAllWindows()
    print("Demo concluded...")
        
def lock_obj(raw_frame, COLOR_ID):
    hsv = cv2.cvtColor(raw_frame, cv2.COLOR_BGR2HSV)
    RED_MASK = cv2.inRange(hsv, R_LOWER, R_UPPER)
    KERNEL = np.ones((5,5), "uint8")
    RED_MASK = cv2.dilate(RED_MASK, KERNEL)
    RES_RED = cv2.bitwise_and(raw_frame, raw_frame, mask = RED_MASK)
    RET, CONTOURS, HIERARCHY = cv2.findContours(RED_MASK, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    records = []
    for x, contour in enumerate(CONTOURS):
        area = cv2.contourArea(contour)
        if area > 300:
            x, y, w, h = cv2.boundingRect(contour)
            records.append([x,y,w,h,area])
    if len(records) == 0:
        return [-1, -1]
    records_sorted = sorted(records, key = lambda x: int(x[4]))
    width = records_sorted[-1][2]
    if width > 330: #230, 260 works #120 works
        return [-2,-2]

    target_x = records_sorted[-1][0] + (records_sorted[-1][2] // 2) 
    target_y = records_sorted[-1][1] + (records_sorted[-1][3] // 2)
    return [target_x, target_y]

def detect_release_ID(region):
    frame = region
    frame_copy = copy.deepcopy(frame)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv_copy = copy.deepcopy(hsv)
    RED_MASK = cv2.inRange(hsv, R_LOWER, R_UPPER)
    KERNEL = np.ones((5,5), "uint8")
    RED_MASK = cv2.dilate(RED_MASK, KERNEL)
    RES_RED = cv2.bitwise_and(frame, frame, mask = RED_MASK)
    RET, CONTOURS, HIERARCHY = cv2.findContours(RED_MASK, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for x, contour in enumerate(CONTOURS):
        area = cv2.contourArea(contour)
        if area > 50:
            return "STATION_RED"
    GREEN_MASK = cv2.inRange(hsv_copy, G_LOWER, G_UPPER)
    GREEN_MASK = cv2.dilate(GREEN_MASK, KERNEL)
    RES_GREEN = cv2.bitwise_and(frame_copy, frame_copy, mask = GREEN_MASK)
    RET2, CONTOURS, HIERARCHY = cv2.findContours(GREEN_MASK, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for x, contour in enumerate(CONTOURS):
        area = cv2.contourArea(contour)
        if area > 50:
            return "STATION_GREEN"
    return False

def lock_station(raw_frame, COLOR_ID):
    hsv = cv2.cvtColor(raw_frame, cv2.COLOR_BGR2HSV)
    BLUE_MASK = cv2.inRange(hsv, B_LOWER, B_UPPER)
    KERNEL = np.ones((5,5), "uint8")
    BLUE_MASK = cv2.dilate(BLUE_MASK, KERNEL)
    RES_BLUE = cv2.bitwise_and(raw_frame, raw_frame, mask = BLUE_MASK)
    RET, CONTOURS, HIERARCHY = cv2.findContours(BLUE_MASK, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    records = []
    for x, contour in enumerate(CONTOURS):
        area = cv2.contourArea(contour)
        if area > 300: #800
            x, y, w, h = cv2.boundingRect(contour)
            #raw_frame = cv2.circle(raw_frame, (x+(w//2), y+(h//2)), radius = 3, color = (0,255,0), thickness = -1)
            
            records.append([x,y,w,h,area])
    if len(records) == 0:
        return [-1, -1]
    for record in records:
        region = raw_frame[record[1]:record[1] + (record[3] // 2), record[0]:record[0] + (record[2] // 2)]
        if detect_release_ID(region) == "STATION_RED" and COLOR_ID == "RED":
            #raw_frame = cv2.circle(raw_frame, (record[0]+(record[2]//2), record[1]+(record[3]//2)), radius = 3, color = (0,0,255), thickness = -1)
            #cv2.putText(raw_frame, "RED RELEASE", (record[0],record[1]), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,0,255))
            if record[2] > 220:
                            return [-2,-2]
            return [record[0]+(record[2]//2), record[1]+(record[3]//2)] 
        elif detect_release_ID(region) == "STATION_GREEN" and COLOR_ID == "GREEN":
            #raw_frame = cv2.circle(raw_frame, (record[0]+(record[2]//2), record[1]+(record[3]//2)), radius = 3, color = (0,255,0), thickness = -1)
            #cv2.putText(raw_frame, "GREEN RELEASE", (record[0],record[1]), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
            return [record[0]+(record[2]//2), record[1]+(record[3]//2)]
        else:
            continue
    return [-1,-1]

def approach_station(COLOR_ID):
    drive_response = 7.26 #6.7#6.65 #7.0 #7.3
    #DRIVE_PWM.start(drive_response)
    time.sleep(0.5)
    DRIVE_PWM.ChangeDutyCycle(drive_response)
    time.sleep(0.1)
    CONSTANT = 1.5 #1.05 #3 2.5 #1.4 # 1.2
    
    SAFETY = 1.
    LOCK_ACQ = 0
    while True:
        __, frame = FRONT_CAM.read()
        target = lock_station(frame, COLOR_ID)
        if target[0] == -2 and target[0] == -2:
            print("TARGET REACHED")
            DRIVE_PWM.stop();
            lower_lift()
            FRONT_CAM.release()
            cv2.destroyAllWindows()
            exit()
        if target[0] == -1 and target[1] == -1:
            cv2.putText(frame, "NO TARGET", (20,50), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
            LOCK_ACQ = 0
        else:
            LOCK_ACQ = 1
            h_stimulus = (target[0] - (WIDTH // 2)) / (WIDTH // 2)
            frame = cv2.circle(frame, (target[0], target[1]), radius = 3, color = (255,255,255), thickness = -1)
            cv2.putText(frame, "H-STIMULUS: " + str(h_stimulus), (20,70), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
        ### CONTROL
        if LOCK_ACQ == 1:
            cv2.putText(frame, "LOCK-ON ACQUIRED", (20,30), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
            if h_stimulus > 0.02 or h_stimulus < -0.02:
                if h_stimulus > 0.02:
                    response = STEER_NEUTRAL - CONSTANT*((STEER_NEUTRAL - STEER_RIGHT_MAX) * h_stimulus)
                    STEER_PWM.ChangeDutyCycle(response)
                    cv2.putText(frame, "STEER RIGHT", (20,50), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
                    cv2.putText(frame, "PWM_SERVO: " + str(response), (20,90), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))  
                else:
                    response = STEER_NEUTRAL + CONSTANT*((STEER_LEFT_MAX - STEER_NEUTRAL) * -1. * h_stimulus) 
                    STEER_PWM.ChangeDutyCycle(response)
                    cv2.putText(frame, "STEER LEFT", (20,50), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
                    cv2.putText(frame, "PWM_SERVO: " + str(response), (20,90), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
            else:
                response = STEER_NEUTRAL
                STEER_PWM.ChangeDutyCycle(response)
                cv2.putText(frame, "STEER NEUTRAL", (20,50), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
                cv2.putText(frame, "PWM_SERVO: " + str(response), (20,90), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
        
        cv2.putText(frame, "LIMITS:: STEER_NEUTRAL: " + str(STEER_NEUTRAL)+" STEER_LEFT_MAX: " + str(STEER_LEFT_MAX) + " STEER_RIGHT_MAX: " + str(STEER_RIGHT_MAX), (20,400), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))                               
        time.sleep(0.020)
        cv2.imshow("APPROACH STATION", frame) 
        if cv2.waitKey(1) & 0xFF == ord('q'):
            FRONT_CAM.release()
            cv2.destroyAllWindows()
            exit()
    
            
def approach_box(COLOR_ID):
    init_us_array()
    time.sleep(0.5)
    drive_response = 7.26 #6.65 6.7#6.65#7#6.65# 6.9#7. #6.98 (too fast 5/04) #6.9 #7.3
    DRIVE_PWM.ChangeDutyCycle(drive_response)
    time.sleep(0.3)
    SAFETY = 1.
    CONSTANT = 1.5 #1.05 #3  #1.4 # 1.2 #2.5
    LOCK_ACQ = 0
    frame_num = 0
    while True:
        __, frame = FRONT_CAM.read()
        target = lock_obj(frame, COLOR_ID)
        if target[0] == -2 and target[0] == -2:
            print("TARGET REACHED")
            STEER_PWM.ChangeDutyCycle(STEER_NEUTRAL)
            DRIVE_PWM.ChangeDutyCycle(DRIVE_NEUTRAL)
            raise_lift()
            time.sleep(2)
            cv2.destroyAllWindows()
            return
    
        if target[0] == -1 and target[1] == -1:
            cv2.putText(frame, "NO TARGET", (20,50), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
            LOCK_ACQ = 0
            response = STEER_NEUTRAL
            STEER_PWM.ChangeDutyCycle(response)
        else:
            #frame = cv2.putText(frame, "DRIVE_PWM: " + str(drive_response), (20, 130), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
            LOCK_ACQ = 1
            h_stimulus = (target[0] - (WIDTH // 2)) / (WIDTH // 2)
            frame = cv2.circle(frame, (target[0], target[1]), radius = 3, color = (255,255,255), thickness = -1)
            cv2.putText(frame, "H-STIMULUS: " + str(h_stimulus), (20,70), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
        if LOCK_ACQ == 1:
            cv2.putText(frame, "LOCK-ON ACQUIRED", (20,30), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
            if h_stimulus > 0.02 or h_stimulus < -0.02:
                if h_stimulus > 0.02:
                    response = STEER_NEUTRAL - CONSTANT*((STEER_NEUTRAL - STEER_RIGHT_MAX) * h_stimulus)
                    STEER_PWM.ChangeDutyCycle(response)
                    cv2.putText(frame, "STEER RIGHT", (20,50), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
                    cv2.putText(frame, "PWM_SERVO: " + str(response), (20,90), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))            
                else:
                    response = STEER_NEUTRAL + CONSTANT*((STEER_LEFT_MAX - STEER_NEUTRAL) * -1. * h_stimulus) 
                    STEER_PWM.ChangeDutyCycle(response)
                    cv2.putText(frame, "STEER LEFT", (20,50), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
                    cv2.putText(frame, "PWM_SERVO: " + str(response), (20,90), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
            else:
                response = STEER_NEUTRAL
                STEER_PWM.ChangeDutyCycle(response)
                cv2.putText(frame, "STEER NEUTRAL", (20,50), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
                cv2.putText(frame, "PWM_SERVO: " + str(response), (20,90), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))
        cv2.putText(frame, "LIMITS:: STEER_NEUTRAL: " + str(STEER_NEUTRAL)+" STEER_LEFT_MAX: " + str(STEER_LEFT_MAX) + " STEER_RIGHT_MAX: " + str(STEER_RIGHT_MAX), (20,400), cv2.FONT_HERSHEY_PLAIN, 1.0, (0,255,0))                               
        time.sleep(0.020)       #0.05, 0.02 work as well
        cv2.imshow("APPROACH BOX", frame) 
        if cv2.waitKey(1) & 0xFF == ord('q'):
            DRIVE_PWM.stop()
            FRONT_CAM.release()
            cv2.destroyAllWindows()
            exit()
    
def approach(mode, color):
    if mode == "PICKUP":
        approach_box(color)
    elif mode == "RELEASE":
        approach_station(color)


if __name__ == "__main__":
    init_lift()
    init_drive()
    init_steer()
    #lower_lift()
    approach(mode="PICKUP", color="RED")
    approach(mode="RELEASE", color ="RED")
    
